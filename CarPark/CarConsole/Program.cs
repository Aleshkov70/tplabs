﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using Logic;

namespace Car
{
    class Program
    {
        static void Main(string[] args)
        {
        CarPark carPark = CarParkFactory.CreateCarPark();

            CarParkPrinter printer = new CarParkPrinter();
            printer.Print(carPark);

            CarParkCalculator carParkCalculator = new CarParkCalculator();
            int totalPrice = carParkCalculator.CalculatePrice(carPark);

            Console.WriteLine("Total CarPark price " + totalPrice);
            Console.ReadKey();
        }
    }
}
