﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class CarPark
    {
        public List<AbstractCar> cars;

        public void AddCar(AbstractCar car)
        {
            if (cars == null)
            {
                cars = new List<AbstractCar>();
            }

            cars.Add(car);
        }

        public List<AbstractCar> Weapons
        {
            get { return cars; }
        }
    }
}
