﻿using System;

public abstract class AbstractCar
{

    private string brand;
    private int price;

    public string Brand
    {
        get { return brand; }
        set { brand = value; }
    }

    public int Price
    {
        get { return price; }
        set { price = value; }
    }
}
