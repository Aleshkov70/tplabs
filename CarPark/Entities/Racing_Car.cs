﻿
public class Racing_Car : AbstractCar
{

    private double max_speed;
    private int max_HP;

    public double Max_speed
    {
        get { return max_speed; }
        set { max_speed = value; }
    }

    public int Max_HP
    {
        get { return max_HP; }
        set { max_HP = value; }
    }
}
